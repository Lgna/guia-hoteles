$(function(){
			$("[data-toggle='tooltip']").tooltip();
			$("[data-toggle='popover']").popover();
			$('.carousel').carousel({
				interval:2000
			});
			$("#contacto").on('show.bs.modal', function(e){
				console.log('el modal se esta usando');
				$("#contactoBtn").prop('disable', 'true');
				$("#contactoBtn").removeClass('btn-outline-success');
				$("#contactoBtn").addClass('btn-primary');
			});
			$("#contacto").on('shown.bs.modal', function(e) {
				console.log("el modal de contacto se mostró");
			});
			$("#contacto").on('hide.bs.modal', function(e) {
				console.log("el modal se esta ocultando");
				$("#contactoBtn").prop('disable', 'false');
				$("#contactoBtn").removeClass('btn-primary');
				$("#contactoBtn").addClass('btn-outline-success');

			});
			$("#contacto").on('hidden.bs.modal', function(e) {
				console.log("el modal de contacto se ocultó");
			});
		});